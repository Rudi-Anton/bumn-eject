import React from "react";
import { StatusBar, Image, MapView, AsyncStorage, Alert, TextInput, ImageBackground, View } from "react-native";
import {
    Button,
    Text,
    Container,
    Card,
    CardItem,
    Body,
    Content,
    Header,
    Title,
    Left,
    Icon,
    Right,
    Footer,
    FooterTab,
    H1,
    Thumbnail,
    Form,
    Item,
    Label,
    Input,
    List,
    ListItem

} from "native-base";

import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';

export default class UbahPassword extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            password: "",
            passwordBaru: "",
            passwordBaru2: ""
        }
    }


    render() {
        return (
            <Container style={{
                marginTop: 25,
                backgroundColor: "white"
            }}>
                <Header style={{ justifyContent: "center", alignContent: "center", backgroundColor: "white" }}>

                    <Left>
                        <Icon name="arrow-back" onPress={() => this.props.navigation.navigate("Profil")} />
                    </Left>
                    <Body>
                        <Title style={{ color: "black" }}>Ubah Password</Title>
                    </Body>
                    <Right/>

                </Header>
                <Content >

                    <Input onChangeText={this.handlePassword} secureTextEntry={true} placeholder='Password Lama' style={{ backgroundColor: "#e8e8e8", marginLeft: "10%", marginRight: "10%", marginTop: "5%", borderRadius: 5, paddingLeft: "5%" }} />
                    <Input onChangeText={this.handlePasswordBaru} secureTextEntry={true} placeholder='Password Baru' style={{ backgroundColor: "#e8e8e8", marginLeft: "10%", marginRight: "10%", marginTop: "1%", borderRadius: 5, paddingLeft: "5%" }} />
                    <Input onChangeText={this.handlePasswordBaru2}secureTextEntry={true} placeholder='Password Baru' style={{ backgroundColor: "#e8e8e8", marginLeft: "10%", marginRight: "10%", marginTop: "1%", borderRadius: 5, paddingLeft: "5%" }} />

                    <Button  onPress={this.ubahPassword} style={{ backgroundColor: "#0052A8", marginLeft: "15%", width: "70%", marginTop: "80%", flex: 1, justifyContent: 'center', alignItems: 'center', borderRadius: 5 }}>
                        <Text>OK</Text>
                    </Button>
                </Content>

            </Container>
        );
    }

    handlePassword = (text) => {
        this.setState({ password: text })
    }
    handlePasswordBaru = (text) => {
        this.setState({ passwordBaru: text })
    }

    handlePasswordBaru2 = (text) => {
        this.setState({ passwordBaru2: text })
    }

    ubahPassword = () => {
        if (this.state.passwordBaru != this.state.passwordBaru2) {
            Alert.alert(
                'Pesan',
                'Password baru tidak sesuai!',
                [
                    //{text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
                    //{text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                    { text: 'OK', style: 'cancel' },
                ],
                { cancelable: false }
            )
        } else if (this.state.passwordBaru.length < 8) {
            Alert.alert(
                'Pesan',
                'Password harus lebih dari 8 huruf/angka',
                [
                    //{text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
                    //{text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                    { text: 'OK', style: 'cancel' },
                ],
                { cancelable: false }
            )
        } else {
            AsyncStorage.getItem("userid", (error, result) => {
                if (result) {
                    console.log("userid : " + result)
                    this.state.userId = result;
                    return fetch("http://ec2-13-229-74-222.ap-southeast-1.compute.amazonaws.com:9000/api/update-password/" + result, {
                        method: 'PUT',
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json',
                            'Authorization': 'Basic dGVsa29tOmRhMWMyNWQ4LTM3YzgtNDFiMS1hZmUyLTQyZGQ0ODI1YmZlYQ=='
                        },
                        body: JSON.stringify({
                            oldPassword: this.state.password,
                            newPassword: this.state.passwordBaru
                        })
                    })
                        .then(response => response.json())
                        .then((data) => {
                            if (data.code == 200) {
                                Alert.alert(
                                    'Pesan',
                                    'Password berhasil diubah',
                                    [
                                        //{text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
                                        //{text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                                        { text: 'OK', onPress: () => {
                                         this.props.navigation.navigate("Profil");
                                     } },
                                    ],
                                    { cancelable: false }
                                )
                               
                            } else{
                                 Alert.alert(
                                    'Pesan',
                                    'Password lama tidak sesuai!',
                                    [
                                        //{text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
                                        //{text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                                        { text: 'OK', style: 'cancel' },
                                    ],
                                    { cancelable: false }
                                )
                            }
                        })
                }
            })
        }


    }

}



